
import java.util.LinkedHashSet;
import java.util.Arrays;
import java.util.Scanner;

public class Program22 {

    static class Final {
        Scanner sc = new Scanner(System.in);
        String MyString = "trimind tech";

        public void removechar() {
            String charremove = MyString.replaceAll("t", " ");
            System.out.println("letter t in trimind tech is deleted = " + charremove);
        }

        public void search() {
            int Index = MyString.indexOf("tech");
            System.out.print(MyString);
            if (Index == -1) {
                System.out.print("\ntech is not found in the string :" + Index);
            } else {
                System.out.print("\ntech is found in the string :" + Index);

            }
        }

        public void CapitalLetter() {
            String words[] = MyString.split("\\s");
            String capitalizeStr = "";
            for (String word : words) {
                String firstLetter = word.substring(0, 1);
                String remainingLetters = word.substring(1);
                capitalizeStr += firstLetter.toUpperCase() + remainingLetters + " ";
            }
            System.out.print("\nCapitalized letter in each word: " + capitalizeStr);
        }

        public void RemoveDublicateWords() {

            String sentence, result = "";
            String allWords[];
            System.out.print("\nEnter your sentence: ");
            sentence = sc.nextLine().toLowerCase();
            allWords = sentence.split(" ");
            LinkedHashSet<String> set = new LinkedHashSet<String>(Arrays.asList(allWords));
            for (String word : set) {
                result = result + word + " ";
            }
            System.out.println("Sentence after removing duplicate words: " + result);
        }

        public void EmailValidation() {
            String EMAIL_REGEX = "^(.+)@trimindtech(.+)$";
            System.out.print("Enter the Email '-work mail only': ");
            String email = sc.nextLine();
            Boolean b = email.matches(EMAIL_REGEX);
            System.out.println("is e-mail: " + email + " :Valid = " + b);
        }

        public void Wordcount() {
            System.out.print("enter the Sentence: ");
            String wordcount = sc.nextLine();
            int count = 1;
            for (int i = 0; i < wordcount.length() - 1; i++) {
                if ((wordcount.charAt(i) == ' ') && (wordcount.charAt(i + 1) != ' ')) {
                    count++;
                }
            }
            System.out.println("Number of words in a string : " + count);
        }
    }

    public static void main(String[] args) {
        Final obj1 = new Final();


        System.out.println("\n1.Remove character from String :");
        System.out.println("\n2.Searching a word in the String :");
        System.out.println("\n3.Capitalizing the  first letter in the word: ");
        System.out.println("\n4.Remove Dublicates words in the sentance");
        System.out.println("\n5.Checking Email valid or not: ");
        System.out.println("\n6.words count in the sentance ");

        Scanner sc=new Scanner(System.in);
        System.out.println("choose the oparation you want : ");
        int n=sc.nextInt();
        switch (n) {
            case 1:
                obj1.removechar();
                break;
            case 2:
                obj1.search();
                break;
            case 3:
                obj1.CapitalLetter();
                break;
            case 4:
                obj1.RemoveDublicateWords();
                break;
            case 5:

                obj1.EmailValidation();

                break;
            case 6:
                obj1.Wordcount();
                break;
            default:
                System.out.println("Please enter a valid input ERROR");

        }
    }
}