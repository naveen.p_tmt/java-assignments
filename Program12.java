package corejava_assignments.src.main.java;

import java.util.Scanner;

public class Program12 {
        public static void main(String[] args) {
            int month;
            int year;
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the below details you want to find the no.of days in the any month in any year");
            System.out.print("Enter the month:");
            month = sc.nextInt();
            System.out.print("Enter the year:");
            year = sc.nextInt();
            switch (month) {
                case 1:
                    System.out.println("Number of days in the January is 31 in the year" + year);
                    break;
                case 2:
                    if (year % 4 == 0) {
                        System.out.println("Number of days in the February is 29 in the year " + year + " hey its Leap Year :) ");
                    } else {
                        System.out.println("Number of days in the February is 28 in the year " + year);
                    }
                    break;
                case 3:
                    System.out.println("Number of days in the March is 31 in the year " + year);
                    break;
                case 4:
                    System.out.println("Number of days in the April is 30 in the year " + year);
                    break;
                case 5:
                    System.out.println("Number of days in the May is 31 in the year " + year);
                    break;
                case 6:
                    System.out.println("Number of days in the June is 30 in the year " + year);
                    break;
                case 7:
                    System.out.println("Number of days in the july is 31 in the year " + year);
                    break;
                case 8:
                    System.out.println("Number of days in the August is 31 in the year " + year);
                    break;
                case 9:
                    System.out.println("Number of days in the September is 30 in the year " + year);
                    break;
                case 10:
                    System.out.println("Number of days in the October is 31 in the year " + year);
                    break;
                case 11:
                    System.out.println("Number of days in the November is 30 in the year " + year);
                    break;
                case 12:
                    System.out.println("Number of days in the December is 31 in the year " + year);
                    break;
                default:
                    System.out.println("please enter valid month in the year " + year);
            }
        }

}
