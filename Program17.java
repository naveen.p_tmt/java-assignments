package corejava_assignments.src.main.java;

public class Program17 {
        public static class methodoverringPerent{
            public void method(){
                System.out.println("hey its a method");
            }
        }
        public static class methodoverring extends methodoverringPerent {
            public void method()
            {
                System.out.println("perent class method is overrided");
            }
        }

        public static class overloading{
            void method1(){
                System.out.println("method1 without paramiters");
            }
        }
        public static class overloding2 extends overloading {
            void method1(int x){
                System.out.println("method1 with 1 paramiter"+x);
            }
        }
        public static class overloding3 extends overloding2 {
            void method1(float y , float z){
                System.out.println("method1 with 2 paramiters "+y+z);
            }
        }

        static class methodhiding{
            public  static void methodhiding1(){
                System.out.print("this is main class");
            }
        }
        public static class methodhiding2 extends methodhiding{
            public static void methodhiding1(){
                System.out.print("this is sub class");
            }
        }

        public static void main(String[] args)
        {
            methodoverring obj=new methodoverring();
            obj.method();
            overloding3 obj2=new overloding3();
            obj2.method1();
            obj2.method1(5);
            obj2.method1(2.5f,7.5f);
            methodhiding2.methodhiding1();
        }

    }


