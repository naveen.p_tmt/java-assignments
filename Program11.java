import java.util.Random;
public class Program11 {
    public static void main(String[]args){
                System.out.println("Generating  vehicle number plate randomly in format of ABC1234");
                Random random = new Random();
                random.ints(65, 90).limit(3)
                        .forEach(a -> System.out.print(Character.toString((char) a)));
                random.ints(48,57).limit(2)
                        .forEach(b ->System.out.print((int)b));
            }
        }
