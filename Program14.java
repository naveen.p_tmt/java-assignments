package corejava_assignments.src.main.java;
import java.util.*;

public class Program14 {
    static class Hashsetclass {
        HashSet<String> hashset = new HashSet();
        HashSet<String> hashset2 = new HashSet();

        void EmptyHashSet() {

            hashset.add("Welcome");
            hashset.add("To");
            hashset.add("trimind");
            hashset.add("tech");
            System.out.println("HashSet: is " + hashset);
            hashset.clear();
            System.out.println("The final set: " + hashset);
        }

        void ConvertHash() {
            hashset2.add("Welcome");
            hashset2.add("To");
            hashset2.add("trimind");
            hashset2.add("tech");
            ArrayList<String> Arreyset = new ArrayList<>(hashset2);
            System.out.println("Elements of Hashset converted into Arraylist  and they are :");
            System.out.println(Arreyset);
        }


        void commonHashelements() {
            HashSet<String> h_set1 = new HashSet<String>();
            h_set1.add("Red");
            h_set1.add("Green");
            h_set1.add("Black");
            h_set1.add("White");
            System.out.println("Frist HashSet content: " + h_set1);
            HashSet<String> h_set2 = new HashSet<String>();
            h_set2.add("Red");
            h_set2.add("Pink");
            h_set2.add("Black");
            h_set2.add("Orange");
            System.out.println("Second HashSet content: " + h_set2);
            h_set1.retainAll(h_set2);
            System.out.println("HashSet content:");
            System.out.println(h_set1);
        }

        void Removehashelements() {
            System.out.println("HashSet is elements are:");
            System.out.println("HashSet: is " + hashset);
            System.out.println("Removing the 'tech ' in hashset :");
            hashset.remove("tech");
            System.out.println("HashSet: is " + hashset);
        }

        void treeset() {
            TreeSet<String> tree = new TreeSet<String>();
            tree.add("Welcome");
            tree.add("To");
            tree.add("trimind");
            tree.add("tech");
            tree.add("tree");
            tree.add("set");
            System.out.println("TreeSet: " + tree);
            System.out.println("The size of the set is: " + tree.size());
        }

        void Hashmap() {
            HashMap<Integer, String> hash_map = new HashMap<Integer, String>();
            hash_map.put(1, "Red");
            hash_map.put(2, "Green");
            hash_map.put(3, "Black");
            hash_map.put(4, "White");
            hash_map.put(5, "Blue");
            Set keyset = hash_map.keySet();
            System.out.println("Key set values are: " + keyset);
        }

    }

    public static void main(String[] args) {
        Hashsetclass obj1 = new Hashsetclass();
        System.out.println("\n1.Empty ah hashset:");
        System.out.println("\n2.convert hashset to arraylist :");
        System.out.println("\n3.comman iteams from the hashset: ");
        System.out.println("\n4.Remove hash elements");
        System.out.println("\n5.Creating a tree set ");

        Scanner sc = new Scanner(System.in);
        System.out.println("choose the oparation you want : ");
        int n = sc.nextInt();
        switch (n) {
            case 1:
                obj1.EmptyHashSet();
                break;
            case 2:
                obj1.ConvertHash();
                break;
            case 3:
                obj1.commonHashelements();
                break;
            case 4:
                obj1.Removehashelements();
                break;
            case 5:

                obj1.treeset();
                break;
            default:
                System.out.println("Please enter a valid input ERROR");

        }
    }
}
