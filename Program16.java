package corejava_assignments.src.main.java;

import java.util.Arrays;

    public class Program16 {

        public static class arrymerge {
            public void arraymethod() {
                float[] array1 = {1.5f, 20.5f, 3.5f, 4.2f, 5.3f};
                float[] array2 = {2.5f, 3.6f, 4.5f, 6.5f, 9.5f};
                int first = array1.length;
                int second = array2.length;
                float[] merge = new float[first + second];
                System.arraycopy(array1, 0, merge, 0, first);
                System.arraycopy(array2, 0, merge, first, second);
                System.out.println(Arrays.toString(merge));
            }
        }
        public  static class dublicate {
            public void arraydiblicate() {

                int [] arr = new int [] {1, 2, 8, 3, 2, 2, 2, 5, 1};
                int [] fr = new int [arr.length];
                int visited = -1;
                for(int i = 0; i < arr.length; i++){
                    int count = 1;
                    for(int j = i+1; j < arr.length; j++){
                        if(arr[i] == arr[j]){
                            count++;
                            //To avoid counting same element again
                            fr[j] = visited;
                        }
                    }
                    if(fr[i] != visited)
                        fr[i] = count;
                }
                for (int i = 0; i < fr.length; i++) {
                    if (fr[i] != visited)
                        System.out.println(" " + arr[i] + " " + fr[i]);
                }
            }
        }

        public static void main(String[] args){
            arrymerge obj1 = new arrymerge();
            obj1.arraymethod();
            dublicate obj2=new dublicate();
            obj2.arraydiblicate();

        }
    }


