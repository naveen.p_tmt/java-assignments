
import java.util.HashMap;
import java.util.Scanner;

import static java.lang.System.out;

public class program2 {
    public static void main(String[] args){
        HashMap<Character,Integer>map=new HashMap<Character,Integer>();
        Scanner sc=new Scanner(System.in);
        out.print("enter the String=");
        String str=sc.nextLine();
        int i=0;
       while(i<str.length()) {
           char currentChar = str.charAt(i);
           if (map.containsKey(currentChar)) {
               map.put(currentChar, map.get(currentChar) + 1);
           } else {
               map.put(currentChar, 1);
           }
           i++;
       }
        System.out.println(map);
    }
}
